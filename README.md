# React Assignment

## Made using

- React
- Redux (Toolkit)
- Formik and Yup

## Usage
- Clone the repository
- `cd` into the repo folder
- Run `npm install` to install all dependencies
- Run `npm start` to run in browser
