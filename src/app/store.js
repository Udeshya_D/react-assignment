import { configureStore } from '@reduxjs/toolkit';
import profileReducer from '../features/profile/profileSlice';

//this is our global store, results from specific reducers are placed
//accordingly
export default configureStore({
	reducer: {
		profile: profileReducer,
	},
});
