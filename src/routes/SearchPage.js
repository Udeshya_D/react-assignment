import React from 'react'
import LocalProfile from '../features/localProfile/localProfile'

//here we can view all the profiles saved in localstorage
const SearchPage = () => {
	return (
		<LocalProfile />
	);
}

export default SearchPage;
