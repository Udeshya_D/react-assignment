import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import './SignupForm.css'

//This is for creating new user

//this function converts as follows: hello_world => Hello World
function humanize(str) {
	var i, frags = str.split('_');
	for (i=0; i<frags.length; i++) {
		frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
	}
	return frags.join(' ');
}

//Since many input fields are repeated, i made a component to reduce code
const TextInputComponent = ({formik, fieldName, type="text", title=null}) => {
	return (
		<div className="input-wrapper">
			<label htmlFor={fieldName}>{title?title:humanize(fieldName)}</label>
			<input
				id={fieldName}
				type={type}
				{...formik.getFieldProps(fieldName)}
			/>
			{formik['touched'][fieldName] && formik['errors'][fieldName] ? (
				<div className="error-input">{formik['errors'][fieldName]}</div>
			) : null}
		</div>
	);
}

//this saves data to localstorage
const saveToLocalStorage = (result) => {
	if (!localStorage.getItem("data")){

		//if localstorage is empty
		let toSave = [result,];
		localStorage.setItem("data", JSON.stringify(toSave));
	} else {
		try{
			let retrieved = JSON.parse(localStorage.getItem("data"));
			retrieved.push(result);
			localStorage.setItem("data", JSON.stringify(retrieved));
		}
		catch(err){
			localStorage.clear("data");
			let toSave = [result,];
			localStorage.setItem('data', JSON.stringify(toSave));
		}
	}
}

//our main form
const SignupForm = () => {
	const formik = useFormik({
		//formik initial values
		initialValues: {
			first_name: '',
			last_name: '',
			dob: '',
			roll_number: '',
			registration_number: '',
			year: '',
			faculty: '',
			batch: '',
			address: '',
			phone: '',

			parent0_name: '',
			parent0_address: '',
			parent0_phone: '',

			parent1_name: '',
			parent1_address: '',
			parent1_phone: '',

			guardian0_name: '',
			guardian0_address: '',
			guardian0_phone: '',

			guardian1_name: '',
			guardian1_address: '',
			guardian1_phone: '',
		},
		//validation using Yup
		validationSchema: Yup.object({
			first_name: Yup.string('Must be string').required('Required'),
			last_name: Yup.string('Must be string').required('Required'),
			dob: Yup.string('Must be string')
			.matches(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/, "Must be in format")
			.required('Required'),
			roll_number: Yup.number('Must be number').required('Required'),
			registration_number: Yup.string('Must be string').required('Required'),
			batch: Yup.number('Must be number').min(1, "Not valid").required('Required'),
			year: Yup.number('Must be number')
			.min(2030, "Must be between 2030 and 2076")
			.max(2076, "Must be between 2030 and 2076")
			.required('Required'),
			faculty: Yup.string("Must be string").required("Required"),
			address: Yup.string('Must be string').required("Required"),
			phone: Yup.string('Must be string').required("Required"),

			parent0_name: Yup.string('Must be string').required("Required"),
			parent0_address: Yup.string('Must be string').required("Required"),
			parent0_phone: Yup.string('Must be string').required("Required"),

			parent1_name: Yup.string('Must be string').required("Required"),
			parent1_address: Yup.string('Must be string').required("Required"),
			parent1_phone: Yup.string('Must be string').required("Required"),

			guardian0_name: Yup.string('Must be string').required("Required"),
			guardian0_address: Yup.string('Must be string').required("Required"),
			guardian0_phone: Yup.string('Must be string').required("Required"),

			guardian1_name: Yup.string('Must be string').required("Required"),
			guardian1_address: Yup.string('Must be string').required("Required"),
			guardian1_phone: Yup.string('Must be string').required("Required"),
		}),
		//do this on submitting
		onSubmit: ( values, {setSubmitting} ) => {
			setSubmitting(true);
			let getPersonData = (property) => {
				//we have two people each for guardian and parents, for making json format easy
				let res = [];
				for (let i=0; i<2; ++i){
					res.push({
						name: values[`${property}${i}_name`],
						address: {
							contact: {
								address: values[`${property}${i}_address`],
								phone: values[`${property}${i}_phone`],
							}
						}
					})
				}
				return res;
			}
			//make in json format and save to local storage in an array
			let result = {
				first_name: values.first_name,
				last_name: values.last_name,
				dob: values.dob,
				roll_number: values.roll_number,
				registration_number: values.registration_number,
				batch: values.batch,
				year: values.year,
				faculty: values.faculty,
				contact: {
					address: values.address,
					phone: values.phone
				},
				parents: getPersonData("parent"),
				guardians: getPersonData("guardian"),
			}

			saveToLocalStorage(result);
			alert("Saved to LocalStorage!");
			setSubmitting(false);
		},
	});

	//form component
	return (
		<div className="form">
			<h3>Create New User!</h3>
			<form onSubmit={formik.handleSubmit} >

				<TextInputComponent 
					formik={formik}
					fieldName={"first_name"} />

				<TextInputComponent 
					formik={formik}
					fieldName={"last_name"} />

				<TextInputComponent 
					formik={formik}
					fieldName={"dob"} />

				<TextInputComponent 
					formik={formik}
					isNumber={true}
					fieldName={"roll_number"} />


				<TextInputComponent 
					formik={formik}
					fieldName={"registration_number"} />


				<TextInputComponent 
					formik={formik}
					type={"number"}
					fieldName={"batch"} />


				<TextInputComponent 
					formik={formik}
					type="number"
					fieldName={"year"} />


				<TextInputComponent 
					formik={formik}
					fieldName={"faculty"} />

				<h3>Contact</h3>

				<TextInputComponent 
					formik={formik}
					fieldName={"address"} />

				<TextInputComponent 
					formik={formik}
					fieldName={"phone"} />

				<h3>Parents information</h3>
				<h4>Parent 1</h4>

				<TextInputComponent 
					formik={formik}
					title="Name"
					fieldName={"parent0_name"} />


				<TextInputComponent 
					formik={formik}
					title="Address"
					fieldName={"parent0_address"} />


				<TextInputComponent 
					formik={formik}
					title={"Phone"}
					fieldName={"parent0_phone"} />

				<h4>Parent 2</h4>

				<TextInputComponent 
					formik={formik}
					title={"Name"}
					fieldName={"parent1_name"} />


				<TextInputComponent 
					formik={formik}
					title={"Address"}
					fieldName={"parent1_address"} />


				<TextInputComponent 
					formik={formik}
					title="Phone"
					fieldName={"parent1_phone"} />


				<h3>Guardian information</h3>

				<h4>Guardian 1</h4>
				<TextInputComponent 
					formik={formik}
					title="Name"
					fieldName={"guardian0_name"} />


				<TextInputComponent 
					formik={formik}
					title="Address"
					fieldName={"guardian0_address"} />


				<TextInputComponent 
					formik={formik}
					title={"Phone"}
					fieldName={"guardian0_phone"} />


				<h4>Guardian 2</h4>

				<TextInputComponent 
					formik={formik}
					title="Name"
					fieldName={"guardian1_name"} />


				<TextInputComponent 
					formik={formik}
					title="Address"
					fieldName={"guardian1_address"} />


				<TextInputComponent 
					formik={formik}
					title={"Phone"}
					fieldName={"guardian1_phone"} />

				<button type="submit">Submit</button>
			</form>
		</div>
	);
};
//phew that was a long form
export default SignupForm;
