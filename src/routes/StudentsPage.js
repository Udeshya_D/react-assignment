import React from 'react'
import {ProfilePresenter} from '../features/profile/profile'

//this is for individual student's page saved in local storage, 
//i have reused <ProfilePresenter /> component used in homepage for reducing code
// Since images were not uploaded in form, there will be error in loading image
const StudentsPage = ({match}) => {
	try{
		let retrieved = JSON.parse(localStorage.getItem('data'));
		for (let i=0; i<retrieved.length; ++i){
			if (retrieved[i].roll_number === match.params.id){
				return (
					<ProfilePresenter data={retrieved[i]} />
				);
			}
		}
		//if student was not found
		return (
			<p>The student does not exist</p>
		)
	}
	catch(e){
		localStorage.removeItem('data');
		return (
			<p>Local Storage was corrupted! Cleared all data</p>
		)
	}
}

export default StudentsPage;
