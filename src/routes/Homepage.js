import React from 'react'
import Profile from '../features/profile/profile'

//this is our homepage where we fetch data and display
const Homepage = () => {
	return (
		<div>
			<Profile />
		</div>
	)
}

export default Homepage;
