import React from "react";
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
} from "react-router-dom";

import HomePage from './routes/Homepage'
import SearchPage from './routes/SearchPage'
import SignupForm from './routes/SignupForm'
import StudentsPage from './routes/StudentsPage'

export default function App() {
	return (
			<Router>
				<div>
					{/* Navbar for our app */}
					<ul>
						<li>
							<Link to="/">Home(Fetch And display)</Link>
						</li>
						<li>
							<Link to="/new-user">Create new user</Link>
						</li>
						<li>
							<Link to="/search">Search Local User</Link>
						</li>
					</ul>

					{/* Render these components on these routes */}
					<Switch>
						<Route path="/students/:id" component={StudentsPage} />
						<Route exact path="/search" component={SearchPage} />
						<Route exact path="/new-user" component={SignupForm} />
						<Route exact path="/" component={HomePage} />
					</Switch>
				</div>
			</Router>
	);
}

