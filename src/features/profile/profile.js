import React, {useEffect} from 'react'
import './profile.css'
import { useSelector, useDispatch } from 'react-redux';

import {
	profileState,
	fetchData,
} from './profileSlice'

//since contact property has specific pattern, i've made a component to for reuse
function ContactPresenter({data}){
	return (
		<div>
			<h3><u>Contact:</u> </h3>
			<dl>
				<dt><b>Address:</b> </dt>
				<dd>{data.address}</dd>
				<dt><b>Phone: </b></dt>
				<dd>{data.phone}</dd>
			</dl>
		</div>
	)
}

//For people like, parents and guardians, it can be reused
function PersonPresenter({data}){
	let peopleList = data.map((item, index) => {
		return (
			<div key={index}>
				<p><b>Name:</b>{item.name}</p>
				<img src={item.avatar} alt={"parent"} />
				<ContactPresenter data={item.address.contact} />
				<hr />
			</div>
		);
	})
	return (
		<div>
			{peopleList}
		</div>
	)
}

//This is for Presenting profile of student if data is obtained successfully
export function ProfilePresenter({data}){
	return (
		<div>
			<div className="img-wrapper">
				<img src={data.avatar} alt={"Student Profile"} />
			</div>
			<p><b>First name: </b>{data.first_name}</p>
			<p><b>Last name: </b>{data.last_name}</p>
			<p><b>Date of birth: </b>{data.dob}</p>
			<p><b>Roll no: </b>{data.roll_number}</p>
			<p><b>Registration number: </b>{data.registration_number}</p>
			<p><b>Batch: </b>{data.batch}</p>
			<p><b>Year: </b>{data.year}</p>
			<p><b>Faculty: </b>{data.faculty}</p>
			<ContactPresenter data={data.contact} />
			<hr />
			<hr />
			<h3>Parents: </h3>
			<PersonPresenter data={data.parents} />
			<hr />
			<h3>Guardians: </h3>
			<PersonPresenter data={data.guardians} />
			<hr />
		</div>
	)
}


//The wrapper outside the profile, to show profile/loading.../failed, according
//to the result of fetch api
export default function Profile(){
	const profilePageState = useSelector(profileState);
	const dispatch = useDispatch();

	useEffect(() => { 
		dispatch(fetchData())
	}, [dispatch]);

	if (!profilePageState.response.didGetResponse){
		return (
			<p>Loading...</p>
		);
	}
	else if (profilePageState.response.didErrorOccur){
		return <p>Error :( </p>
	}
	else {
		return (
			<ProfilePresenter data={profilePageState.studentData.payload} />
		)
	}
}
