import { createSlice } from '@reduxjs/toolkit';

//this is redux toolkit for defining actions and reducers, reducers are under
//the property 'reducers', and initialState, and name are given accordingly
//createSlice automatically configures type of action, and relates them to reducer
export const profileSlice = createSlice({
	name: 'profile',
	initialState: {
		response: {
			didGetResponse: false,
			didErrorOccur: false,
		},
		studentData: null,
	},
	reducers: {
		//if client successfully fetches data, store is updated accordingly
		setSuccess: (state, studentData) => {
			state.response.didGetResponse = true;
			state.response.didErrorOccur = false;
			state.studentData = studentData;
		},
		//if fails to get data, or connection is lost
		setError: state => {
			state.response.didGetResponse = true;
			state.response.didErrorOccur = true;
		}
	}
});

//destructuring for better use in profile.js
export const {setSuccess, setError} = profileSlice.actions;

//async logic for fetching data
//this is flexible, since any we can pass any student's profile into props
//and it'll fetch accordingly
export const fetchData = () => dispatch => {
	fetch('https://run.mocky.io/v3/291c5c08-af6d-42ac-bfae-65e575938138')
		.then(res => res.json())
		.then(data => dispatch(setSuccess(data)))
		.catch(error => dispatch(setError()))
}

//redux toolkit for passing specific part of store to specific comoponent
export const profileState = state => state.profile;

export default profileSlice.reducer;
