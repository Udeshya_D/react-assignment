import React from 'react'
import {
	Link
} from 'react-router-dom'
import './localProfile.css'

//for presenting list of student's profiles
const LocalProfilePresenter = ({data}) => {
	return (
		data.map((item, index) => (
			<div key={index} className={'student'}>
				<p>Roll number: {item.roll_number}</p>
				<p>Name: {item.first_name}</p>
				<Link to={`/students/${item.roll_number}`}>Profile</Link>
			</div>
		))
	)
}

//display the list after fetching from local storage
const LocalProfile = () => {
	try{
		let retrieved = JSON.parse(localStorage.getItem('data'));
		if (!retrieved){
			return (
				<p>There are no any items in local storage</p>
			);
		} else {
			return (
				<LocalProfilePresenter data={retrieved} />
			)
		}
	}
	catch{
		return (
			<p>Data in local storage is corrupted, please clear local storage</p>
		)
	}
}

export default LocalProfile;
